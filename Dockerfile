# I should probably be building from a specific version
# I think it's acceptable to build from external sources, if I specify version.
# At least while I'm one guy with no dog. If I were doing this for real,
# I would copy this image into my own, Cloud-hosted docker repo, and build
# from that repo. 
FROM jupyter/datascience-notebook

WORKDIR /usr/src/app
RUN pip install voila
COPY . .

CMD [ "voila", "hello-world.ipynb", "--port", "8080"]
